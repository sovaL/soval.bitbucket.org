var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var ehrId;

var datum = [];
var visina = [];
var teza = [];
var temperatura = [];
var pulz = [];
var tlakS = [];
var tlakD = [];

var HS = [];
var HHS = 0;
var vMinus = 0;
var vPlus = 0;

var dataGrafi = [];

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();

	var ime = $("#novoIme").val();
	var priimek = $("#novPriimek").val();
  var datumRojstva = $("#novRD").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
          $("#content").hide();
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
		        console.log(ehrId);
		        $("#trenutniEHR").val(ehrId);
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                /* $("#content").show(); */
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#trenutniEHR").val(ehrId);
		                }
		            },
		            error: function(err) {
		                $("#content").hide();
		                $("#trenutniEHR").val("");
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	ehrId = $("#trenutniEHR").val();

	if (!ehrId || ehrId.trim().length == 0) {
	    $("#content").hide();
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    	    $("#content").show();
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " + "label-success fade-in'>Bolnik '" + party.firstNames + " " + party.lastNames + "' ('" + party.dateOfBirth +") je bil uspešno vpisan.</span>");
				$("#imePriimek").html("Health Score za osebo " + party.firstNames + " " + party.lastNames);
				posodobiGrafe();
			},
			error: function(err) {
			    $("#content").hide();
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
    console.log("Začetek dodajanja meritve");
	sessionId = getSessionId();

	//ehrId = $("#trenutniEHR").val();
	var telesnaVisina = $("#dodajVisina").val();
	var telesnaTeza = $("#dodajTeza").val();
	var telesnaTemperatura = $("#dodajTemperatura").val();
	var srcniUtrip = $("#dodajPulz").val();
	var sistolicniKrvniTlak = $("#dodajTlakS").val();
	var diastolicniKrvniTlak = $("#dodajTlakD").val();
	var merilec = "zdravnik";

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    //"ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/pulse:0/any_event:0/rate|magnitude":srcniUtrip,
		    "vital_signs/pulse:0/any_event:0/rate|unit":"/min",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		if (telesnaVisina=="" || telesnaTeza=="" || telesnaTemperatura=="" || srcniUtrip=="" || sistolicniKrvniTlak=="" || diastolicniKrvniTlak=="") {
		    $("#poljaSporocilo").html("<span class='obvestilo label label-warning " + "fade-in'>Prosim izpolnite vsa polja!");
		}
		else{
    		$.ajax({
    		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    		    type: 'POST',
    		    contentType: 'application/json',
    		    data: JSON.stringify(podatki),
    		    success: function (res) {
    		    	posodobiGrafe();
    		        //console.log("Dela");
    		        $("#poljaSporocilo").html(
                  "<span class='obvestilo label label-success fade-in'>Podatki vnešeni</span>");
    		    },
    		    error: function(err) {
    		        console.log("Ni šlo skos");
    		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
    		    }
    		});    
		}
	}
}

function posodobiGrafe(){
	datum = [];
	visina = [];
	teza = [];
	temperatura = [];
	pulz = [];
	tlakS = [];
	tlakD = [];
	ehrId = $("#trenutniEHR").val();
	HS = [];
	HHS = 0;
	vMinus = 0;
	vPlus = 0;
	
	$("#hs").html("");
	$("#hhs").html("Nalagam...");
	$.ajax({
		url: baseUrl + "/view/" + ehrId + "/" + "height",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
			//console.log("visina");
			if (res.length > 0) {
				for (var i in res) {
					datum.unshift(res[i].time);
					visina.unshift(res[i].height);
					//console.log(res[i].time);
					//console.log(res[i].height);
				}
			}
			else{
				console.log("ne dela");
			}
		}
	});
	
	$.ajax({
		url: baseUrl + "/view/" + ehrId + "/" + "weight",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
			if (res.length > 0) {
				//console.log("teza");
				for (var i in res) {
					teza.unshift(res[i].weight);
					//console.log(res[i].time);
					//console.log(res[i].weight);
				}
			}
			else{
				console.log("ne dela");
			}
		}
	});
	
	$.ajax({
		url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
			if (res.length > 0) {
				//console.log("temperatura");
				for (var i in res) {
					temperatura.unshift(res[i].temperature);
					//console.log(res[i].time);
					//console.log(res[i].temperature);
				}
			}
			else{
				console.log("ne dela");
			}
		}
	});
	$.ajax({
		url: baseUrl + "/view/" + ehrId + "/" + "pulse",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
			if (res.length > 0) {
				//console.log("pulz");
				for (var i in res) {
					pulz.unshift(res[i].pulse);
					//console.log(res[i].time);
					//console.log(res[i].pulse);
				}
			}
			else{
				console.log("ne dela");
			}
		}
	});
	$.ajax({
		url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
			if (res.length > 0) {
				for (var i in res) {
					tlakS.unshift(res[i].systolic);
					tlakD.unshift(res[i].diastolic);
					//console.log(res[i].time);
					//console.log(res[i].systolic);
					//console.log(res[i].diastolic);
				}
			}
			else{
				console.log("ne dela");
			}
		}
	});
	//code before the pause
	setTimeout(function(){
		if(datum.length >0){
			napolniHS();
		    /*HS.toString();
		    $("#hs").html(HS);*/
		    izracuajHHS();
		    $("#hhs").html("<h1 class='center'> Vaš HHS: " + HHS + "</h1>");
		    narediData();
		    drawChart();
		    drawLineChart();
		}
		else{
			$("#graf").html("Ni obstoječih veljavnih podatkov");
			$("#HHSkrog").html("Ni obstoječih veljavnih podatkov");
			$("#hhs").html("");
		}
	    
	}, 2000);
}

/**
 * Napolne tabelo z izracunanimi podatki za vso zgodovino.
 */
function napolniHS(){
	for (var i in datum) {
		var odstej = 0;
		var bmi = teza[i]/(visina[i]*visina[i]/10000);
		
		//bmi
		if(bmi > 35){
			odstej+=30;
		}
		else if(bmi > 30){
			odstej+=20;	
		}
		else if(bmi > 25){
			odstej+=10;
		}
		else if(bmi > 18.5){
		}
		else{
			odstej+=25;
		}
		
		//temperatura
		if(temperatura[i] > 38.5){
			odstej+=40;
		}
		else if(temperatura[i] > 37.5){
			odstej+=25;	
		}
		else if(temperatura[i] > 37){
			odstej+=10;
		}
		else if(temperatura[i] > 35.8){
		}
		else{
			odstej+=15;
		}
		
		//pulz
		if(pulz[i] > 82){
			odstej+=10;
		}
		else if(pulz[i] > 76){
			odstej+=7;	
		}
		else if(pulz[i] > 70){
			odstej+=5;
		}
		else if(pulz[i] > 67){
			odstej+=2
		}
		
		//pritisk S
		if(tlakS[i] > 133){
			odstej+=10;
		}
		else if(tlakS[i] <110){
			odstej+=5;	
		}
		
		//pritisk D
		if(tlakD[i] > 84){
			odstej+=10;
		}
		else if(tlakD[i] <75){
			odstej+=5;	
		}
		
		
		HS[i] = 100 - odstej;
	}
}

function narediData(){
	dataGrafi=[];
	var len = HS.length;
	for (var i = 0; i < len; i++) {
		//console.log(typeof datum[i]);
		var d = datum[i].substr(0,10);
	    dataGrafi.push([d, HS[i]]);
	}
	if(len > 1){
		var razlika = dataGrafi[len-1][1] - HHS;
		if(razlika == 0){
			vPlus = dataGrafi[len-1][1] - dataGrafi[len-2][1];
			vMinus = 0;
			$("#hs").html("Health High Score ste povečali za " + vPlus +" točk!");
		}
		else{
			vPlus = 0;
			vMinus = -razlika;
			$("#hs").html("Za vašim Health High Socore trenutno zaostajate za " + vMinus +" točk!");
		}
	}
}

/**
 * V tabeli HS poišče največjo vrednost - HHS
 */
function izracuajHHS(){
	HHS = Math.max.apply(null, HS);
	
}



function preberiPacienta(){
    var pacient = document.getElementById("preberiPacienta").value;
    console.log(pacient);
    switch(pacient){
    	case '1':
    		ehrId = '6d0b9fb0-cfbe-43a2-8e50-4972df2ee046';
		    $("#trenutniEHR").val(ehrId);
    		break;
    	case '2':
    		ehrId = 'dc20c391-d84b-410f-8375-80e06afaec6d';
		    $("#trenutniEHR").val(ehrId);
    		break;
    	case '3':
    		ehrId = '25a3ed92-ec74-4748-8d18-51f824ad1f56';
		    $("#trenutniEHR").val(ehrId);
    		break;
    	case '0':
		    $("#trenutniEHR").val("");
    		break;
    	default:
    		console.log(pacient);
		    $("#trenutniEHR").val(pacient);
    }
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	var ehrId = "";
	sessionId = getSessionId();
  
	var ime;
	var priimek;
	var RD;
	var telesnaVisina;
	var telesnaTeza;
	var telesnaTemperatura;
	var srcniUtrip;
	var sistolicniKrvniTlak;
	var diastolicniKrvniTlak;

	switch(stPacienta){
    	case 1:
			ime = "Zdravko";
			priimek = "Zdravić";
			RD = "1990-01-01T00:00:00.000Z";
			telesnaVisina = "170";
			telesnaTeza = "70";
			telesnaTemperatura = "36.3";
			srcniUtrip = "60";
			sistolicniKrvniTlak = "120";
			diastolicniKrvniTlak = "80";
	    break;
	    case 2:
	        ime = "Normalka";
			priimek = "Obična";
			RD = "1990-02-02T00:00:00.000Z";
			telesnaVisina = "160";
			telesnaTeza = "65";
			telesnaTemperatura = "36.5";
			srcniUtrip = "68";
			sistolicniKrvniTlak = "125";
			diastolicniKrvniTlak = "81";
	    break;    
	    case 3:
	        ime = "Blanka";
			priimek = "Bolanka";
			RD = "1990-03-03T00:00:00.000Z";
			telesnaVisina = "165";
			telesnaTeza = "65";
			telesnaTemperatura = "38.5";
			srcniUtrip = "77";
			sistolicniKrvniTlak = "140";
			diastolicniKrvniTlak = "885";
	    break;
	}
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
        url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        ehrId = data.ehrId;
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: RD,
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    alert("Uspešno generiranje osebe: "+ime+" "+priimek+", ehrID: "+ ehrId);
	                    $("#preberiPacienta").append("<option value="+ehrId+">"+ime+" "+priimek+" (zgeneriran)</option>");
	                    	var podatki = {
                            		    "ctx/language": "en",
                            		    "ctx/territory": "SI",
                            		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
									    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
									   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
									    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
									    "vital_signs/pulse:0/any_event:0/rate|magnitude":srcniUtrip,
									    "vital_signs/pulse:0/any_event:0/rate|unit":"/min",
									    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
									    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
                            };
                            var parametriZahteve = {
                            		    ehrId: ehrId,
                            		    templateId: 'Vital Signs',
                            		    format: 'FLAT',
                            		    committer: "zdravnik"
                            };
                            $.ajax({
                                url: baseUrl + "/composition?" + $.param(parametriZahteve),
                            	type: 'POST',
                            	contentType: 'application/json',
                            	data: JSON.stringify(podatki),
                            	success: function (res) {},
                            	error: function(err) {
                            		alert(JSON.parse(err.responseText).userMessage);   	
                            	}
                            });                
                }
            },
	        error: function(err) {
	           alert('Napaka: '+ JSON.parse(err.responseText).userMessage);
	       }
	   });
	  }
	});
}

function generiraneOsebe(){
	generirajPodatke(1);
	
    setTimeout(function(){
    	generirajPodatke(2);
    },500);
    
	setTimeout(function(){
		generirajPodatke(3);
	},1000);
}

/**
 * Izvede, ko se stran naloži
 */
 
$(document).ready(function() {
    $("#content").hide();


});

//konec