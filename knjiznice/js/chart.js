google.charts.load('current', {packages: ['corechart', 'line']});

function drawLineChart() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'X');
      data.addColumn('number', 'HS');

      data.addRows(dataGrafi);

      var options = {
        hAxis: {
          title: 'Datum',
        },
        vAxis: {
          title: 'HS'
        },
        height:200,
        backgroundColor: '#ffffff'
      };

      var chart = new google.visualization.LineChart(document.getElementById('graf'));
      chart.draw(data, options);
    }