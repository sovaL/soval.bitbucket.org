      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     HHS - vPlus - vMinus],
          ['',     vMinus],
          ['',     vPlus],
          ['',     100 -HHS]
        ]);

        var options = {
          pieHole: 0.8,
          pieSliceText: 'none',
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          slices: {
            0: { color: '#286090' },
            1: { color: '#cf5965'},
            2: { color: '#52ab64'},
            3: { color: '#f5f5f5' }
          },
          height:200,
        };

        var chart = new google.visualization.PieChart(document.getElementById('HHSkrog'));
        chart.draw(data, options);
      }